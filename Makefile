@:
	@echo "install/remove rules only. use: make install or make remove. One variable: DESTDIR, base dir for installs"
	@echo "debian-patch, will adjust rules for debian based systems. run this before install on debian and ubuntu"
	
install:
	install -Dm 644 rsyslog/10-ninja-ids.conf  "$(DESTDIR)/etc/rsyslog.d/10-ninja-ids.conf"
	install -Dm 644 logrotate/ninja-ids "$(DESTDIR)/etc/logrotate.d/ninja-ids"
	install -Dm 644 logrotate/arp_watch "$(DESTDIR)/etc/logrotate.d/arp_watch"
	install -Dm 644 iptables/ninja-ids.rules "$(DESTDIR)/etc/iptables/ninja-ids.rules"
	install -Dm 644 iptables/ninja-ids-6.rules "$(DESTDIR)/etc/iptables/ninja-ids-6.rules"
	install -Dm 644 iptables/iptables_user_rules "$(DESTDIR)/etc/iptables/iptables_user_rules"
	install -Dm 644 psad/psad.conf "$(DESTDIR)/usr/share/ninjaos/psad.conf"
	install -Dm 755 ninja-ids-ctl.sh "$(DESTDIR)/usr/bin/ninja-ids-ctl.sh"
	install -Dm 755 fw_tcpdump.sh "$(DESTDIR)/usr/bin/fw_tcpdump.sh"
	install -Dm 755 fw_nc.sh "$(DESTDIR)/usr/bin/fw_nc.sh"
	install -Dm 755 fw_hold.sh "$(DESTDIR)/usr/bin/fw_hold.sh"
	install -Dm 755 arp_watch.py "$(DESTDIR)/usr/bin/arp_watch.py"
	install -Dm 755 iptables-user.sh "$(DESTDIR)/usr/bin/iptables-user.sh"
	install -Dm 644 bash-completion/ninja-ids-ctl.bash "$(DESTDIR)/usr/share/bash-completion/completions/ninja-ids-ctl.sh"
	install -Dm 644 bash-completion/fw_nc.bash "$(DESTDIR)/usr/share/bash-completion/completions/fw_nc.sh"
	install -Dm 644 bash-completion/fw_tcpdump.bash "$(DESTDIR)/usr/share/bash-completion/completions/fw_tcpdump.sh"
	install -Dm 644 man/fw_nc.1 "$(DESTDIR)/usr/share/man/man1/fw_nc.1"
	install -Dm 644 man/fw_tcpdump.1 "$(DESTDIR)/usr/share/man/man1/fw_tcpdump.1"
	install -Dm 644 man/fw_hold.1 "$(DESTDIR)/usr/share/man/man1/fw_hold.1"
	install -Dm 644 man/arp_watch.1 "$(DESTDIR)/usr/share/man/man1/arp_watch.1"
	install -Dm 644 man/ninja-ids-ctl.8 "$(DESTDIR)/usr/share/man/man8/ninja-ids-ctl.8"
	install -Dm 644 man/iptables-user.8 "$(DESTDIR)/usr/share/man/man8/iptables-user.8"
	install -Dm 644 systemd_units/arp_watch.service "$(DESTDIR)/usr/lib/systemd/system/arp_watch.service"
	install -Dm 644 systemd_units/arp_watch.default "$(DESTDIR)/etc/default/arp_watch"
	install -Dm 644 systemd_units/iptables_user.service "$(DESTDIR)/usr/lib/systemd/system/iptables_user.service"
remove:
	rm -f "$(DESTDIR)/etc/rsyslog.d/10-ninja-ids.conf"
	rm -f "$(DESTDIR)/etc/logrotate.d/ninja-ids"
	rm -f "$(DESTDIR)/etc/logrotate.d/arp_watch"
	rm -f "$(DESTDIR)/etc/iptables/ninja-ids.rules"
	rm -f "$(DESTDIR)/etc/iptables/ninja-ids-6.rules"
	rm -f "$(DESTDIR)/etc/iptables/iptables_user_rules"
	rm -f "$(DESTDIR)/usr/share/ninjaos/psad.conf"
	rm -f "$(DESTDIR)/usr/bin/ninja-ids-ctl.sh"
	rm -f "$(DESTDIR)/usr/bin/fw_tcpdump.sh"
	rm -f "$(DESTDIR)/usr/bin/fw_nc.sh"
	rm -f "$(DESTDIR)/usr/bin/fw_hold.sh"
	rm -f "$(DESTDIR)/usr/bin/arp_watch.py"
	rm -f "$(DESTDIR)/usr/bin/iptables-user.sh"
	rm -f "$(DESTDIR)/usr/share/bash-completion/completions/ninja-ids-ctl.sh"
	rm -f "$(DESTDIR)/usr/share/bash-completion/completions/fw_nc.sh"
	rm -f "$(DESTDIR)/usr/share/bash-completion/completions/fw_tcpdump.sh"
	rm -f "$(DESTDIR)/usr/share/man/man1/fw_nc.1"
	rm -f "$(DESTDIR)/usr/share/man/man1/fw_tcpdump.1"
	rm -f "$(DESTDIR)/usr/share/man/man1/fw_hold.1"
	rm -f "$(DESTDIR)/usr/share/man/man1/arp_watch.1"
	rm -f "$(DESTDIR)/usr/share/man/man8/ninja-ids-ctl.8"
	rm -f "$(DESTDIR)/usr/share/man/man8/iptables-user.8"
	rm -f "$(DESTDIR)/usr/lib/systemd/system/arp_watch.service"
	rm -f "$(DESTDIR)/etc/default/arp_watch"
	rm -f "$(DESTDIR)/usr/lib/systemd/system/iptables_user.service"
debian-patch:
	sed -i 's|/etc/iptables/iptables.rules|/etc/iptables/rules.v4|g' ninja-ids-ctl.sh
	sed -i 's|/etc/iptables/ip6tables.rules|/etc/iptables/rules.v6|g' ninja-ids-ctl.sh
	sed -i 's|iptables ip6tables|netfilter-persistent|g' ninja-ids-ctl.sh
	sed -i 's|/usr/bin/iptables|/usr/sbin/iptables|g' psad/psad.conf
	sed -i 's|/usr/bin/ip6tables|/usr/sbin/ip6tables|g' psad/psad.conf
	sed -i 's|/usr/bin/ifconfig|/usr/sbin/ifconfig|g' psad/psad.conf
	sed -i 's|/usr/bin/ip|/usr/sbin/ip|g' psad/psad.conf
	sed -i 's|/usr/bin/whois_psad|/usr/sbin/whois_psad|g' psad/psad.conf
	sed -i 's|/usr/bin/fwcheck_psad|/usr/sbin/fwcheck_psad|g' psad/psad.conf
	sed -i 's|/usr/bin/psadwatchd|/usr/sbin/psadwatchd|g' psad/psad.conf
	sed -i 's|/usr/bin/kmsgsd|/usr/sbin/kmsgsd|g' psad/psad.conf
	sed -i 's|/usr/bin/psad|/usr/sbin/psad|g' psad/psad.conf
