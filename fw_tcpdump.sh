#!/usr/bin/env bash
# exit codes 0-success, 1-script error, 2-user error, 4-help

help_and_exit(){
  cat 1>&2 << EOF
${BRIGHT}fw_tcpdump.sh${NOCOLOR}: firewall tcpdump

Open port(s) in the firewall, run tcpdump, and then remove the firewall rule.

Script gets interface, port and proto from tcpdump statement, no special
options, just use tcpdump syntax.

NOTE: the less specific you get with your syntax, the wider the firewall rules
will be opened

	${BRIGHT}USAGE${NOCOLOR}:
	fw_tcpdump.sh <tcpdump statement>
EOF
  exit 4
}

BRIGHT=$(tput bold)
NOCOLOR=$(tput sgr0) #reset to default colors
BRIGHT_RED=$(tput setaf 1;tput bold)
BRIGHT_YELLOW=$(tput setaf 3;tput bold)
BRIGHT_CYAN=$(tput setaf 6;tput bold)

exit_with_error(){
  echo 1>&2 "${BRIGHT}fw_tcpdump.sh${NOCOLOR}: ${BRIGHT_RED}ERROR${NOCOLOR}: ${2}"
  exit ${1}
}
message(){
  echo "${BRIGHT}fw_tcpdump.sh${NOCOLOR}: ${@}"
}
check_sudo(){
  # Check if this script can run sudo correctly. uses as_root, see below
  if [ ${UID} -eq 0 ];then
    ROOT_METHOD="uid"
   # TODO: FIX Polkit support
   #elif [[ ! -z $DISPLAY  && $(tty) = /dev/pts/* ]];then
   # ROOT_METHOD="pkexec"
   elif [ $(sudo whoami ) == "root" ];then
    ROOT_METHOD="sudo"
   else
    exit_with_error 4 "Cannot gain root! This program needs root to work Exiting..."
  fi
  # one last check
  [ $( as_root whoami ) != "root" ] && exit_with_error 4 "Cannot gain root! This program needs root to work Exiting..."
}
as_root(){
  # execute a command as root.
  case $ROOT_METHOD in
   sudo)
    sudo ${@}
    ;;
   polkit)
    pkexec ${@}
    ;;
   uid)
    ${@}
    ;;
  esac
}
check_port_and_proto() {
  local item
  PORT=""
  PROTO=""
  IFACE=""
  while [ ! -z "$1" ];do
    item="${1,,}"
    case ${item} in
     port)
      if [ -z $PORT ];then
        PORT="${2}"
       else
        PORT+=",${2}"
      fi
      shift
      ;;
     tcp)
      PROTO="tcp"
      ;;
     udp)
      PROTO="udp"
      ;;
     icmp)
      PROTO="icmp"
      ;;
     --interface)
      IFACE=${2}
      shift
      ;;
     -*)
      if [[ ${1} = *i* ]];then
        IFACE=${2}
        shift
      fi
    esac
    shift
  done
}

remove_rule_and_exit() {
  as_root iptables "-D ${COMMAND}"
  exit
}

main() {
  # Sanity Check
  [ "${1}" == "--help" ] && help_and_exit 
  # Parse options. use, protocol, port and interface if given
  if [[ -z ${PORT} && -z ${PROTO} ]];then
    COMMAND="INPUT -j ACCEPT"
   elif [[ -z ${PORT} && ! -z ${PROTO} ]];then
    COMMAND="INPUT -m ${PROTO} -p ${PROTO} -j ACCEPT"
   else
    COMMAND="INPUT -m multiport -p ${PROTO} --dports ${PORT} -j ACCEPT"
  fi
  [ ${IFACE} ] && COMMAND+=" -i ${IFACE}"
  check_sudo
  as_root iptables "-I ${COMMAND}"
  trap remove_rule_and_exit 1 2 3 9 15
  tcpdump ${@}
  remove_rule_and_exit
}

check_port_and_proto "${@}"
main "${@}"
