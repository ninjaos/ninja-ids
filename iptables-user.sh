#!/usr/bin/env bash
#
# IPtables user override exceptions. These rules are applied after the main IDS
# Logging rules. use /etc/iptables/iptables_override to edit the actual rules

# exit codes: 0-success, 1-program error, 2-user error, 4-help

IPTABLES=$(which iptables)
IP6TABLES=$(which ip6tables)
RULES_FILE="/etc/iptables/iptables_user_rules"

BRIGHT=$(tput bold)
NOCOLOR=$(tput sgr0) #reset to default colors
BRIGHT_RED=$(tput setaf 1;tput bold)
BRIGHT_YELLOW=$(tput setaf 3;tput bold)
BRIGHT_CYAN=$(tput setaf 6;tput bold)
BRIGHT_PURPLE=$(tput setaf 5;tput bold)
BRIGHT_GREEN=$(tput setaf 2;tput bold)
BRIGHT_WHITE=$(tput setaf 7;tput bold)

# load rules files
if [ -f  "${RULES_FILE}" ];then
  source "${RULES_FILE}"
 else
  exit_with_error 2 "Could not load ${RULES_FILE}. Check that it exists, and is readable"
fi

message() {
  echo "${BRIGHT}iptables_user.sh${NOCOLOR}: ${@}"
}
submsg(){
  echo "${BRIGHT}[${BRIGHT_CYAN}+${BRIGHT_WHITE}]${NOCOLOR}	${@}"
}

exit_with_error(){
  echo 1>&2 "${BRIGHT}iptables_user.sh${NOCOLOR}: ${BRIGHT_RED}ERROR${NOCOLOR}: ${2}"
  exit ${1}
}

warn(){
  echo 1>&2 "${BRIGHT_CYAN}[${NOCOLOR}${BRIGHT}+${BRIGHT_CYAN}] ${BRIGHT_YELLOW}WARN${NOCOLOR}:	${@}"
}

strip_comments() {
  # Takes variable of file lines(i.e. with cat) and returns them comment stripped
  # first comment is name of output variable for printf -c
  local out_var=${1}
  shift
  local IFS="
"
  local in_lines="${@}"
  local out_lines=""
  local i_line=""
  for line in ${in_lines[@]};do
    line="$(cut -d "#" -f1 <<< ${line} )"
    [ ! -z "${line}" ] && out_lines+="${line}\n"
  done
  printf -v ${out_var} %b "${out_lines}"
}

main() {
  message "Loading Rules From ${RULES_FILE}..."
  declare -i ERRORS=0
  local IFS="
"
  submsg "IPv4"
  strip_comments IPV4_DECOM "${IPV4RULES}"
  for line in ${IPV4_DECOM[@]};do
    IFS=" "
    ${IPTABLES} ${line}
    if [ ${?} -ne 0 ];then
      warn "Failed: ${line}"
      ERRORS+=1
      continue
     else
      echo "${line}"
    fi
  done
  IFS="
"
  submsg "IPv6"
  strip_comments IPV6_DECOM "${IPV6RULES}"
  for line in ${IPV6_DECOM[@]};do
    IFS=" "
    ${IP6TABLES} ${line}
    if [ ${?} -ne 0 ];then
      warn "Failed: ${line}"
      ERRORS+=1
      continue
     else
      echo "${line}"
    fi
  done
  if [ $ERRORS -gt 0 ];then
    exit_with_error 1 "There where $ERRORS error(s), see above..."
   else
    exit 0
  fi
}

main "${@}"
