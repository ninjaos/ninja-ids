# ninja-ids

Intrusion Detection System for Ninja OS. Lightweight IDS for Desktop Linux to
detect people mucking around the local network or doing reconnessaince/basic
network level attacks against the host.

Current tools: iptables, rsyslog, and psad

In house tools:
arp_watch	Our own arp table monitoring solution, comes with systemd unit

Designed for Archlinux, but now has a makefile rule for running on Debian(and
derivatives)

Vision
------


Stage 1.
create logs

Stage 2.
parse logs

stage 3
create user interface for alerting. Desktop version with systray and Qt 5 applet
for digging through logs

PROGRESS:
We are still in stage 1, but making progress


TODO:
Components needed:

Wireless/802.11* - passively recognize and detect most common attacks and if
they are targeted, at what, and if its targeted at localhost

Ethernet/LAN - detect things like monitoring/spoofing, ARP SCAN DONE

Log parsers - look for attacks against top level applications. email, web, proxy
etc...

Alerting
Desktop - Qt5 console, systray,popups, etc...
server - email??
both - logs in /var/log/ninja-ids/

Logs
----
Logs output to ```/var/log/ninja-ids/```

Scripts
-------
For your convience the following scripts are provided

ninja-ids-ctl.sh - enable or disable the configurations and update signatures.

iptables-user.sh - loads additional IPTables rules on top of the rules used
by Ninja IDS.

Loads rules from /etc/iptables/iptables\_user\_rules. This file can be tracked
independantly.

fw_ scripts: These scripts open a port or ports on the firewall, run a command
and then close the port when they are done. They pass the syntax of the original
command on to said command and use this to generate firewall rules.

fw\_tcpdump.sh - firewall tcpdump.

fw\_nc.sh - firewall netcat.

fw\_hold.sh - Arbitrarily hold open the firewall without running a program.
Program stays running until terminated, and then closes firewall ports. see
--help

Workflow:
---------
iptables is setup to monitor all blocked packets, with rules for unusual packets
like fin/syn christmas and null packets. These are all given easy to grep for
logging prefixes.

user additions and overrides such as to allow running daemons or services
to the firewall can be done in ```/etc/iptables/iptables_user_rules```

this is controlled with the iptables_user.service script.

rsyslog has rules set up to look for these prefixes and write them to
/var/log/ninja-ids/iptables.log. Because this logs EVERY SINGLE REJECTED PACKET,
there is a logrotate rule set to cut the log at 50 MB size.

PSAD is set up to watch /var/log/ninja-ids/iptables.log and then log portscans
and stats, but not do any blocking or reporting.

arp_watch monitors the arp-table and optionally for changes and suspicious
activity and logs to /var/log/ninja-ids/arp_watch.log. It also displays internal
tables in /var/lib/arp_tables/. it optionally can use arp-scan for active
scanning.

Install
--------

1a. NEW: If you are running debian or debian based distros like Ubuntu or raspian:
run debian-patch before make install to correct path names for debian
```
make debian-patch
```

1. use provided Makefile to install files. this has a DESTDIR variable for
chroots

```
make install
```

2. Enable configs and restart daemons.
```
ninja-ids-ctl.sh enable
```

