#!/usr/bin/env bash
# exit codes 0-success, 1-script error, 2-user error, 4-help

help_and_exit(){
  cat 1>&2 << EOF
${BRIGHT}fw_hold.sh:${NOCOLOR} Firewall Hold

Arbirarily hold open port(s) on the iptables firewall until cancelled.

When the program runs, an exception is made using iptables, when the program
exits, the rule is removed. The program will run until killed, either with ^C,
or with the kill command.

A statement is constructed via keywords. A direction keyword is manditory,
and only one direction statement is allowed. The rest may be added as seen fit.

NOTE: That more keywords is more restrictions on the firewall hole. Less
keywords is a bigger hole.
 
	${BRIGHT}USAGE${NOCOLOR}:
	fw_hold.sh <statement>
	
	${BRIGHT}KEYWORDS${NOCOLOR}:
	help		This message(exits with no operations)
	
		DIRECTIONAL:
	----------------------------------------------
	in		incomming traffic(INPUT chain)
	
	out		outgoing traffic(OUTPUT chain)
	
	fwd		"forwarded", i.e. router or bridge(FORWARD chain)
	
		OPERATIONAL:
	----------------------------------------------
	udp		Only use the UDP protocol
	
	tcp		Only use the TCP protocol
	
	icmp		Only use the ICMP protocol
	
	port <N>	Port(s). Follow same syntax as iptables, allows
			multiples and ranges, e.x. 80,443,6660-7000. Comma
			seperated, without spaces.
			
	icmp-type	For use with the ICMP protocol. Manditory if icmp is
			specified. See iptables documentation for more info.
	
	iface <iface>	name of network interface
	
	log		Continue to log packets with IDS(LOG_ACCEPT rule)

EOF
  exit 4
} 

BRIGHT=$(tput bold)
NOCOLOR=$(tput sgr0) #reset to default colors
BRIGHT_RED=$(tput setaf 1;tput bold)
BRIGHT_YELLOW=$(tput setaf 3;tput bold)
BRIGHT_CYAN=$(tput setaf 6;tput bold)
BRIGHT_PURPLE=$(tput setaf 5;tput bold)
BRIGHT_GREEN=$(tput setaf 2;tput bold)

exit_with_error(){
  echo 1>&2 "${BRIGHT}fw_hold.sh${NOCOLOR}: ${BRIGHT_RED}ERROR${NOCOLOR}: ${2}"
  exit ${1}
}
message(){
  echo "${BRIGHT}fw_hold.sh${NOCOLOR}: ${@}"
}
warn(){
  echo 1>&2 "${BRIGHT_CYAN}[${NOCOLOR}${BRIGHT}+${BRIGHT_CYAN}] ${BRIGHT_YELLOW}WARN${NOCOLOR}:	${@}"
}

check_sudo(){
  # Check if this script can run sudo correctly. uses as_root, see below
  if [ ${UID} -eq 0 ];then
    ROOT_METHOD="uid"
   # TODO: FIX Polkit support
   #elif [[ ! -z $DISPLAY  && $(tty) = /dev/pts/* ]];then
   # ROOT_METHOD="pkexec"
   elif [ $(sudo whoami ) == "root" ];then
    ROOT_METHOD="sudo"
   else
    exit_with_error 4 "Cannot gain root! This program needs root to work!"
  fi
  # one last check
  [ $( as_root whoami ) != "root" ] && exit_with_error 4 "Cannot gain root! This program needs root to work!"
}
as_root(){
  # execute a command as root.
  case $ROOT_METHOD in
   sudo)
    sudo ${@}
    ;;
   polkit)
    pkexec ${@}
    ;;
   uid)
    ${@}
    ;;
  esac
}

check_port_and_proto() {
  # que machine that checks syntax
  local item
  PORT=""
  PROTO=""
  IFACE=""
  DIRECTION=""
  ICMP_TYPE=""
  ACCEPT="ACCEPT"
  local -i dir_statements=0

  # Check for help
  [ "${1}" == "--help" ] && help_and_exit
  [ -z "${1}" ] && help_and_exit

  while [ ! -z "$1" ];do
    item="${1,,}"
    case ${item} in
     help)
      help_and_exit
      ;;
     port)
      PORT="${2}"
      shift
      ;;
     icmp-type)
      ICMP_TYPE="${2}"
      shift
      ;;
     tcp)
      PROTO="tcp"
      ;;
     udp)
      PROTO="udp"
      ;;
     icmp)
      PROTO="icmp"
      ;;
     iface)
      IFACE="${2}"
      shift
      ;;
     in)
      DIRECTION="INPUT"
      dir_statements+=1
      ;;
     out)
      DIRECTION="OUTPUT"
      dir_statements+=1
      ;;
     fwd)
      DIRECTION="FORWARD"
      dir_statements+=1
      ;;
     log)
      ACCEPT="LOG_ACCEPT"
      ;;
     *)
      warn "${item}: invalid keyword"
      ;;
    esac
    shift
  done
  ## Check conflicts
  # direction
  [ ${dir_statements} -gt 1 ] && exit_with_error 2 "More than one directional keyword(in,out,fw). Needs to be precisely one of these, see help"
  [ -z ${DIRECTION} ] && exit_with_error 2 "No direction keyword(in,out,fwd) was made. See help for usage"
  # protocol stuff
  [[ -z ${PROTO} ]] && PROTO="tcp"
  [[ "${PROTO}" == "icmp" && -z "${ICMP_TYPE}" ]] && exit_with_error 2 "IPtables needs icmp-type for icmp protocol. See help for usage"
}

remove_rule_and_exit() {
  # When we are done, remove the rule and exit
  echo ""
  as_root iptables "-D ${COMMAND}" || exit_with_error 1 "Could not remove firewall rules: ${COMMAND}"
  message "${BRIGHT_GREEN}Firewall ${BRIGHT_YELLOW}Hold ${BRIGHT_PURPLE}Released${NOCOLOR}"
  exit

}

main() {

  TICK=5 # Time in seconds main loop goes. relvatively long because it doesn't do anything
  
  # Build command from options
  if [[ -z ${PORT} && -z ${PROTO} ]];then
    COMMAND="${DIRECTION} -j ${ACCEPT}"
   elif [ "${PROTO}" == "icmp" ];then
    COMMAND="${DIRECTION} -m icmp -p icmp --icmp-type ${ICMP_TYPE} -j ${ACCEPT}"]
    unset PORT # prevent from echo'ing
   elif [[ -z ${PORT} && ! -z ${PROTO} ]];then
    COMMAND="${DIRECTION} -m ${PROTO} -p ${PROTO} -j ${ACCEPT}"
   else
    COMMAND="${DIRECTION} -m multiport -p ${PROTO} --dports ${PORT} -j ${ACCEPT}"
  fi
  [ ${IFACE} ] && COMMAND+=" -i ${IFACE}"
  
  # pop rooot
  check_sudo
  
  # Everything should look good at this point. Generate the banner message
  banner_msg="${BRIGHT_PURPLE}Holding ${BRIGHT_YELLOW}Open ${BRIGHT_GREEN}Firewall${NOCOLOR}:	Direction:${BRIGHT_CYAN}${DIRECTION}${NOCOLOR}"
  [ ${IFACE} ] && banner_msg+="	Interface:${BRIGHT_CYAN}${IFACE}${NOCOLOR}" 
  [ ${PROTO} ] && banner_msg+="	Protocol:${BRIGHT_CYAN}${PROTO}${NOCOLOR}"
  [ ${PORT} ] && banner_msg+="	Port(s):${BRIGHT_CYAN}${PORT}${NOCOLOR}"
  [ ${ICMP_TYPE} ] && banner_msg+="	ICMP-type:${BRIGHT_CYAN}${ICMP_TYPE}${NOCOLOR}"
  [ ${ACCEPT} == "LOG_ACCEPT" ] && banner_msg+="	${BRIGHT}Logging Enabled${NOCOLOR}"
  message "${banner_msg}"

  # Open port on firewall
  as_root iptables "-I ${COMMAND}" || \
    exit_with_error 1 "could not set iptables rule, check syntax: -I $COMMAND"
  # Trap escapes. This ensures we cleanup on exit
  trap remove_rule_and_exit 1 2 3 9 15

  # Main loop, keep the program running
  while true;do
    sleep ${TICK}
  done
  
  # Clean up and exit
  remove_rule_and_exit
}

check_port_and_proto "${@}"
main "${@}"
