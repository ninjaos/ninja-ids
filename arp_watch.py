#!/usr/bin/env python
prog_desc='''arp_watch.py
This python program/daemon for monitoring the arp tables for changes and other
abnormal behavior. Default behavior is to monitor "arp", but active scans with
"arp-scan" can be done as well. If you specify IP addresses on the command line,
watch will limit to these. Otherwise everything on the LAN will be monitored.

Current table will be in /var/arp_watch/arp-table

MAC addresses with multiple IPs will be in /var/arp_watch/dupe-table

This program needs to be run as root. you should likely start with it attached
system .service file
'''
prog_desc = prog_desc.strip()

import netifaces
import argparse
import signal
import sys, os
import subprocess
import time

options = {
    'progname'    : os.path.basename(sys.argv[0]),
    'logfile'     : '/var/log/ninja-ids/arp_watch.log',
    'arp_table'   : '/var/lib/arp_watch/arp-table',
    'dupe_table'  : '/var/lib/arp_watch/dupe-table',
    'activescan'  : False,
    'flushcache'  : False,
    'check_int'   : 0.5,
    'scan_int'    : 10,
    'long_tick'   : None, # scan_int / check_int
}

class colors:
    bold='\033[01m'
    reset='\033[0m'
    brightred='\033[91m'
    brightgreen='\033[92m'
    yellow='\033[93m'
    brightcyan='\033[96m'

def exit_with_error(code,message):
    '''Exit with an code and message'''
    print("arp_watch.py:" + colors.brightred + " ERROR: " + colors.reset + message,file=sys.stderr)
    sys.exit(code)

def report_log(text_string):
    '''print an abritrary string to the logs/console'''
    line = time.asctime() + ' ' + options['progname'] + ": " + text_string
    print(line)
    outfile = open(options['logfile'],"a")
    outfile.write(line+"\n")
    outfile.close()

def clear_arp_cache():
    '''Clears all entries out of the arp cache. takes no parameters and gives back no output '''
    try:
        subprocess.call(['ip', 'neigh', 'flush', 'all'])
    except:
        message="ERROR: Could not clear arp cache"
        report_log(message)


def check_passive(address_list):
    '''Generates an arp_table in code from the arp command. Takes an address_list []. If null list[], the entire table is used'''
    errors = 0
    # two dicts, first is for ip-mac pairs, and second is for ip-interface
    arp_table = [{},{}]  
    if address_list == []:
        try:
            arp_output = subprocess.check_output(["arp","-na"])
        except:
            exit_with_error(1,"Could not run arp -na, please check envornment.")
        arp_output = arp_output.decode()
        arp_output = arp_output.split('\n')
        for line in arp_output:
            line = line.split()
            if len(line) < 4:
                continue
            ip_addr,mac_addr,iface = line[1],line[3],line[-1]
            ip_addr = ip_addr.strip("()")
            # update arp table, 0 is ip-mac table, 1 is ip-interface table
            arp_table[0].update({ip_addr:mac_addr})
            arp_table[1].update({ip_addr:iface})
    else:
        for address in address_list:
            try:
                arp_output = subprocess.check_output(["arp","-na", address])
            except:
                errors += 1
                continue
            arp_output = arp_output.decode()
            arp_output = arp_output.strip()
            if "no match found" in arp_output:
                continue
            arp_output = arp_output.split()
            ip_addr,mac_addr,iface = arp_output[1],arp_output[3],arp_output[-1]
            ip_addr = ip_addr.strip("()")
            arp_table[0].update({ip_addr:mac_addr})
            arp_table[1].update({ip_addr:iface})
    return arp_table

def gen_dupe_table(arp_table):
    '''Check for duplicates, MAC addresses with more than 1 IP, takes the arp_table as input'''
    dupe_table = {}
    values = list( arp_table[0].values() )
    dupes = set()
    # Generate list of duplicate MACs
    for mac in values:
        if values.count(mac) > 1:
            dupes.add(mac)
    # Now report on duplicates
    for item in dupes:
        ip_list = []
        for ip, mac in arp_table[0].items():
            if mac == item:
                ip_list.append(ip)
        dupe_table.update({item,ip_list})
    
    return dupe_table

def check_sniffers(iface_list=None):
    '''Check for network sniffers by arp-scanning FF:FF:00:00:00:00. Takes a list of iface names as input. If None or [], get from netifaces'''
    '''Based on this post: https://twitter.com/dildog/status/1427817959683248131'''
    bcast_target = 'ff:ff:00:00:00:00'
    if iface_list != None or iface_list != []:
        iface_list = get_iface_list()
    scan_timeout = "3" # in Seconds.
    arp_table = [{},{}]

    for iface in iface_list:
        try:
            arp_output = subprocess.check_output(["arp-scan",'--timeout',scan_timeout,'--interface',iface,"--localnet",'--destaddr',bcast_target])
        except:
            exit_with_error(1,"check-sniffers: Could not run arp-scan, please check envornment.")
            
        arp_output = arp_output.decode()
        arp_output = arp_output.split('\n')
        # chop off everything but data
        arp_output = arp_output[2:]
        arp_output = arp_output[:-4]
        for line in arp_output:
            line = line.split('\t')
            ip_addr,mac_addr = line[0],line[1]
            arp_table[0].update({ip_addr:mac_addr})
            arp_table[1].update({ip_addr:iface})
    return arp_table
    
def get_iface_list():
    '''Return list of Ethernet interfaces with assigned IPs from netiface'''
    iface_use_list = []
    iface_list = netifaces.interfaces()
    for iface in iface_list:
        # Skip loopback
        if iface == "lo":
            continue
        # AF_INET means it has an IP address, AF_LINK means it has an IP address. these resolve to 2 and 17 respectively.
        if netifaces.AF_INET in netifaces.ifaddresses(iface) and netifaces.AF_LINK in netifaces.ifaddresses(iface):
            iface_use_list.append(iface)
    return iface_use_list
            
def check_active(address_list):
    '''Actively scan, using arp-scan. Takes an address list[]'''
    scan_timeout = "3" # in Seconds.
    arp_table = [{},{}]

    # get a list of active ethernet interfaces
    iface_use_list = get_iface_list()
    
    for iface in iface_use_list:
        if address_list == []:
            try:
                arp_output = subprocess.check_output(["arp-scan",'--timeout',scan_timeout,'--interface',iface,"--localnet"])
            except:
                exit_with_error(1,"active-scan: Could not run arp-scan, please check envornment.")
        else:
            try:
                arp_output = subprocess.check_output(["arp-scan","--timeout",scan_timeout,'--interface',iface," ".join(address_list)])
            except:
                exit_with_error(1,"active-scan: Could not run arp-scan, please check envornment.")
        arp_output = arp_output.decode()
        arp_output = arp_output.split('\n')
        # chop off everything but data
        arp_output = arp_output[2:]
        arp_output = arp_output[:-4]
        for line in arp_output:
            line = line.split('\t')
            ip_addr,mac_addr = line[0],line[1]
            arp_table[0].update({ip_addr:mac_addr})
            arp_table[1].update({ip_addr:iface})

    return arp_table

def arp_table_report(arp_table,old_arp_table):
    '''print and log updates to the arp table. takes both the new, then old arp table as parameters '''
    if old_arp_table == None:
        # If there is no existing arp table, all IPs are new
        for ip in arp_table[0]:
            message = "New Entry: IP_Address: " + ip + " MAC_Address: " + arp_table[0][ip] + " on Interface: " + arp_table[1][ip]
            report_log(message)
    else:
        for ip in old_arp_table[0]:
            # IP address has been deleted
            if ip not in arp_table[0]:
                message = "Remove Entry: IP_Address: " + ip + " MAC_Address: " + old_arp_table[0][ip] + " on Interface: " + old_arp_table[1][ip]
                report_log(message)
                continue
            # Check if MAC address changes
            if old_arp_table[0][ip] != arp_table[0][ip]:
                message ="Mac Address Change: IP_Address: " + ip + " Old MAC_Address: " + old_arp_table[0][ip] + " New MAC_Address: " + arp_table[0][ip] + " on Interface: " + arp_table[1][ip]
                report_log(message)
            # Check if interface changed
            if old_arp_table[1][ip] != arp_table[1][ip]:
                message ="Interface Change: IP_Address: " + ip + " MAC_Address: " + arp_table[0][ip] + " From Interface:" + old_arp_table[1][ip] + " to Interface: " + arp_table[1][ip]
                report_log(message)
            # Check if IP address is new
        for ip in arp_table[0]:
            if ip not in old_arp_table[0]:
                message = "New Entry: IP_Address: " + ip + " MAC_Address: " + arp_table[0][ip] + " on Interface: " + arp_table[1][ip]
                report_log(message)

def main_loop(address_list):
    '''Where the action is'''

    # initialize ARP tables
    arp_table        = None
    dupe_table       = None
    old_arp_table    = None
    old_dupe_table   = None
    active_arp_table = [{},{}]
    
    # Long tick is the amount of loops needed for active scan(s)
    long_tick = options['long_tick']
    
    while True:
        # Passive check, and updated with previous active entries
        arp_table = check_passive(address_list)
        arp_table[0].update(active_arp_table[0])
        arp_table[1].update(active_arp_table[1])
        # Now report
        arp_table_report(arp_table,old_arp_table)
        
        # Long tick. This happens on scan_int time in seconds. Trigger long tick events, and reset the counter
        if long_tick >= options['long_tick']:
            long_tick = 0
            if options['flushcache'] == True:
                clear_arp_cache()
            if options['activescan'] == True:
                # do the arp scan, report new IPs, then update the table
                active_arp_table = check_active(address_list)
                arp_table_report(active_arp_table,arp_table)
                arp_table[0].update(active_arp_table[0])
                arp_table[1].update(active_arp_table[1])

        # Once we've gotten all the arps, check for dupes from combined list
        dupe_table = gen_dupe_table(arp_table)
                
        #now do the same for dupes
        if old_dupe_table == None:
            for mac in dupe_table:
                message = "New Entry: Duplicate MAC_Address: " + mac + " IP_Addresses: " + " ".join(dupe_table[mac])
                report_log(message)
        else:
            for mac in old_dupe_table:
                if mac not in dupe_table:
                    message = "Remove Entry: Duplicate MAC_Address: " + mac + " IP_Addresses: " + " ".join(old_dupe_table[mac])
                    report_log(message)
                elif old_dupe_table[mac] != dupe_table[mac]:
                    message = "Update Entry: Duplicate MAC_Address: " + mac + " IP_Addresses " + " ".join(dupe_table[mac])
                    report_log(message)
            for mac in dupe_table:
                if mac not in old_dupe_table:
                    message = "New Entry: Duplicate MAC_Address: " + mac + " IP_Addresses: " + " ".join(dupe_table[mac])
                    report_log(message)

        ## Write ARP table
        # Generate arp-table output
        output = ""
        for ip in arp_table[0]:
            # ip address, mac address, and interface respectively from the arp table, and a new line
            line    = ip + "\t" + arp_table[0][ip] + "\t" + arp_table[1][ip] + "\n"
            output += line
        # Now write it
        arp_table_file = open(options['arp_table'],'w')
        arp_table_file.write(output)
        arp_table_file.close()
        
        # Generate Dupte Table
        output = ""
        for mac in dupe_table:
            # MAC address, and then list of IP adddresses from the dupe table and a new line
            line    = mac + "\t" + " ".join(dupe_table[mac]) + "\n"
            output += line
        dupe_table_file = open(options['dupe_table'],"w")
        dupe_table_file.write(output)
        dupe_table_file.close()
        
        # what was new, is now old
        old_arp_table  = arp_table
        old_dupe_table = dupe_table
        
        long_tick += 1
        time.sleep(options['check_int'])

def cleanup_and_exit(exit_code,frame):
    '''Stop program and shut down'''
    report_log("Shutting Down")
    os.remove(options['arp_table'])
    os.remove(options['dupe_table'])
    sys.exit(exit_code)

def main():
    '''main program'''
    #yarg parser. get ye options ye scallywag
    parser = argparse.ArgumentParser(description=prog_desc,epilog="\n\n",add_help=False,formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("ip_addr", nargs="*"   , help="Optional: IP addresses to monitor.")
    parser.add_argument("-?", "--help"         , help="Show This Help Message", action="help")
    parser.add_argument("-a", "--active-scan"  , help="Actively scan with arp-scan",action="store_true")
    parser.add_argument("-f", "--flush-cache"  , help="Clear the arp cache every SCAN_INTERVAL. Best with active scan",action="store_true")
    parser.add_argument("-i", "--scan-interval", help="Interval in seconds between active scans, DEFAULT: 10",type=int)
    parser.add_argument("-l", "--log-file"     , help="Path and filename of Logfile",type=str)
    args = parser.parse_args()

    # startup batter
    if args.ip_addr != []:
        message = "Staring up. checking " + " ".join(args.ip_addr) + " in the ARP Table"
    else:
        message = "Staring up. Watching Everything in the ARP Table"
    # proccess ye options
    if args.active_scan != None or args.log_file != None or args.scan_interval != None:
        message += "\n[+] Options: "
    if args.active_scan == True:
        message += "Active_Scan "
        options['activescan'] = True
    if args.flush_cache == True:
        options['flushcache'] == True
        message += "Flush_cache "
    if args.log_file != None:
        message += "Log_File: " + args.log_file
        options['logfile'] = args.log_file
    if args.scan_interval != None:
        message += "Scan_interval(Seconds): " + args_scan_interval
        options['scan_int'] = args.scan_interval

    report_log(message)
    
    # Calculate "Long Tick", the amount of loops needed for the active scan
    # Rounded down, even though that shouldn't be an issue with check_int being
    # .5, and scan_int being an int type, they should always evenly divide
    options['long_tick'] = int( options['scan_int'] / options['check_int'] )

    #check if we are running as root
    if os.getuid() != 0:
        exit_with_error(2,"Not root! This program needs to be run as root! see --help for options.")
    # graceful shutdown on interrupts
    signal.signal(signal.SIGINT,cleanup_and_exit)
    signal.signal(signal.SIGTERM,cleanup_and_exit)
    
    # Make sure log file dir exists
    log_file_dir = os.path.dirname(options['logfile'])
    if os.path.exists(log_file_dir) == False:
        os.mkdir(log_file_dir)
    
    # Setup arp table. This file only exists while program is running
    arp_table_dir = os.path.dirname(options['arp_table'])
    if os.path.exists(arp_table_dir) == False:
        os.mkdir(arp_table_dir)
    arp_table_file = open(options['arp_table'],'w')
    arp_table_file.close()
    dupe_table_file  = open(options['dupe_table'],'w')
    dupe_table_file.close()
    
    # Where the action is
    main_loop(args.ip_addr)
    
    # When we are done
    cleanup_and_exit(0,None)

if __name__ == "__main__":
    main()
