#!/usr/bin/env bash
# exit codes 0-success, 1-script failure, 2-bad input, 4-help

SNORT_RULES_URL="https://rules.emergingthreats.net/open/snort-2.9.0/"

NOCOLOR=$(tput sgr0) #reset to default colors
BRIGHT=$(tput bold)
BRIGHT_RED=$(tput setaf 1;tput bold)
BRIGHT_CYAN=$(tput setaf 6;tput bold)
BRIGHT_YELLOW=$(tput setaf 3;tput bold)
BRIGHT_WHITE=$(tput setaf 7;tput bold)

help_and_exit(){
  cat 1>&2 << EOF
${BRIGHT}ninja-ids-ctl.sh${NOCOLOR}:
Control "Ninja IDS". Ninja OS's Intrusion Detection Software. Work in Progress

As of now it is just rsyslog, iptables, and psad configs that work with
eachother. In the future this will grow for more monitoring and detection.

	${BRIGHT}DEPEDENCIES:${BRIGHT} psad, rsyslog, and iptables

	${BRIGHT}USAGE${NOCOLOR}:
	ninja-ids-ctl.sh ${BRIGHT_YELLOW}<${NOCOLOR}enable${BRIGHT_CYAN}|${NOCOLOR}disable${BRIGHT_CYAN}|${NOCOLOR}update${BRIGHT_CYAN}|${NOCOLOR}reload${BRIGHT_CYAN}|${NOCOLOR}help${BRIGHT_YELLOW}>${NOCOLOR}
	
	${BRIGHT}COMMANDS:${NOCOLOR}

	enable		Activates Ninja-IDS by disabling current configs and
			symlinking ninja-ids configs.
			
	disable		Removes symlinks and restores previous configs.
	
	update		Update rules and signatures(PSAD)
	
	reload		restart relevant services/daemons. reload systemd, then
			in order, iptables, rsyslog, and then psad
			
	iptables-update	Reload new iptables rules, restarts iptables/ip6tables
			first, and then reloads iptables_user afterwards

EOF
  exit 4
}

message(){
  echo "${BRIGHT}ninja-ids-ctl.sh${NOCOLOR}: ${@}"
}

submsg(){
  echo "${BRIGHT}[${BRIGHT_CYAN}+${BRIGHT_WHITE}]${NOCOLOR}	${@}"
}

warn(){
  echo 1>&2 "${BRIGHT}[${BRIGHT_YELLOW}+${BRIGHT_WHITE}] ${BRIGHT_YELLOW}WARN:${NOCOLOR}	${@}"
}
exit_with_error(){
  echo 1>&2 "ninja-ids-ctl.sh: ERROR: ${2}"
  exit ${1}
}
as_root(){
  # execute a command as root.
  case $ROOT_METHOD in
   sudo)
    sudo ${@}
    ;;
   polkit)
    pkexec ${@}
    ;;
   uid)
    ${@}
    ;;
  esac
}

check_sudo(){
  # Check if this script can run sudo correctly. uses as_root, see below
  if [ ${UID} -eq 0 ];then
    ROOT_METHOD="uid"
   # TODO: FIX Polkit support
   #elif [[ ! -z $DISPLAY  && $(tty) = /dev/pts/* ]];then
   # ROOT_METHOD="pkexec"
   elif [ $(sudo whoami ) == "root" ];then
    ROOT_METHOD="sudo"
   else
    exit_with_error 4 "Cannot gain root! This program needs root to work Exiting..."
  fi
  # one last check
  [ $( as_root whoami ) != "root" ] && exit_with_error 4 "Cannot gain root! This program needs root to work Exiting..."
}

dep_check(){
  # check binary dependencies. If these aren't installed script will fail
  local bin_deps="iptables ip6tables rsyslogd psad"
  for file in ${bin_deps};do
    which $file &> /dev/null || exit_with_error 1 "${file} not in PATH, check that it is installed. aborting!"
  done
}

ninja_enable(){
  # Install all relevant configs, enable services and reload daemons so it takes effect.
  # These are symlink'd so they can be installed/uninstalled without deleting them
  # and using ls will show what they are doing.

  message "Enabling Ninja-ids"

  # Check for files
  [ -f /usr/share/ninjaos/psad.conf ] || exit_with_error 1 "PSAD config not in /usr/share/ninjaos/psad.conf"  
  [ -f /etc/iptables/ninja-ids.rules ] || exit_with_error 1 "iptables config not in /etc/iptables"
  [ -f /etc/iptables/ninja-ids-6.rules ] || exit_with_error 1 "ip6tables config not in /etc/iptables"
  [ -f /etc/iptables/iptables_user_rules ] || exit_with_error 1 "iptables user rules not in /etc/iptables/iptables_user_rules"

  submsg "Copying Files, Making Backups"
  as_root mv /etc/psad/psad.conf /etc/psad/psad.conf.ninja-bak || warn "Could not backup of psad.conf"
  as_root mv /etc/iptables/iptables.rules /etc/iptables/iptables.rules.ninja-bak || warn "Could not backup of iptables.conf"
  as_root mv /etc/iptables/ip6tables.rules /etc/iptables/ip6tables.rules.ninja-bak || warn "Could not backup of ip6tables.conf"
  as_root ln -s /etc/iptables/ninja-ids.rules /etc/iptables/iptables.rules || warn "Could not setup iptables rules"
  as_root ln -s /etc/iptables/ninja-ids-6.rules /etc/iptables/ip6tables.rules || warn  "Could not setup ip ip6tables rules"
  as_root ln -s /usr/share/ninjaos/psad.conf /etc/psad/psad.conf || warn "Could not setup psad config"
  as_root mkdir -p /var/log/audit
  
  submsg "Adjusting Configs"
  as_root sed -i "s/+HOSTNAME+/${HOSTNAME}/g" /usr/share/ninjaos/psad.conf

  submsg "Enable System Services"
  as_root systemctl enable iptables ip6tables iptables_user || warn "Could not enable IPtables"
  as_root systemctl enable rsyslog || warn "Couldn't enable Rsyslog"
  as_root systemctl enable psad || warn "Could not enable PSAD"
  as_root systemctl enable arp_watch || warn "Could not enable arp_watch"
  ninja_reload
}

ninja_disable(){
  # If there was any backed up configs from before installing Ninja-IDS, restore those.
  message "Disabling Ninja-ids"
  submsg "Restoring old configs"
  if [ -f /etc/psad/psad.conf.ninja-bak ];then
    as_root rm /etc/psad/psad.conf 
    as_root mv /etc/psad/psad.conf.ninja-bak /etc/psad/psad.conf || warn "Restoration of old psad.conf failed!"
  fi
  if [ -f /etc/iptables/iptables.rules.ninja-bak ];then
    as_root rm /etc/iptables/iptables.rules
    as_root mv /etc/iptables/iptables.rules.ninja-bak /etc/iptables/iptables.rules || warn "Restoration of old iptables.rules failed!"
  fi
  if [ -f /etc/iptables/ip6tables.rules.ninja-bak ];then
    as_root rm /etc/iptables/ip6tables.rules
    as_root mv /etc/iptables/ip6tables.rules.ninja-bak /etc/iptables/ip6tables.rules || warn "Restoration of old iptables.rules failed!"
  fi
  
  # Disabling ninja-ids specific daemons
  as_root systemctl disable arp_watch
  as_root systemctl disable iptables_user
  message "Restarting Services..."
  as_root systemctl daemon-reload || warn "Could not reload systemd"
  as_root systemctl restart iptables ip6tables || warn "Could not restart iptables. check rules files"
  as_root systemctl restart rsyslog || warn "Could not restart rsyslog"
  as_root systemctl restart psad || warn "Could not restart PSAD"
  as_root systemctl stop arp_watch || warn "arp_watch stop failed"
  as_root systemctl stop iptables_user || warn "could not stop iptables user rules"
}

ninja_update() {
  # Update rules and definitions
  local snort_rules_file="emerging-all.rules.tar.gz"
  local snort_rules_url="${SNORT_RULES_URL}/${snort_rules_file}"
  
  message "Updating Rules and Definitions"
  #local temp_dir=$(mktemp -d)

  #submsg "Updating SNORT rules"
  #local -i snort_errors=0
  # Get snort rules
  #curl ${snort_rules_url} --output "${temp_dir}/${snort_rules_file}"  &> /dev/null || snort_errors+=1
  #curl ${snort_rules_url}.md5 --output "${temp_dir}/${snort_rules_file}.md5" &> /dev/null || snort_errors+=1
  #if [ ${snort_errors} -gt 0 ];then
  # warn "Download of snort rules FAILED!"
  # return 1
  #fi
  # Check snort rules
  #local snort_rules_md5=$(md5sum "${temp_dir}/${snort_rules_file}" | cut -d " " -f 1)
  #local snort_md5_file=$(cat "${temp_dir}/${snort_rules_file}.md5")
  #if [ ${snort_rules_md5} !=  ${snort_md5_file} ];then
  #  warn "Snort rules MD5 check fails!"
  #  return 1
  #fi
  # extract rules file
  #as_root tar zxf "${temp_dir}/${snort_rules_file}" -C /etc/psad/snort_rules/ || warn "Could not extract snort rules!"
  #cleanup
  #as_root rm -rf "${temp_dir}" || warn "Could not cleanup temp directory. Check this manually ${temp_dir}"

  submsg "Updating PSAD rules"
  as_root psad --sig-update &> /dev/null || warn "PSAD rules update FAILED!"
  
  submsg "Restarting PSAD"
  as_root systemctl restart psad || warn "PSAD restart FAILED, check logs and journalctl -xe"
}

ninja_reload() {
  # Reload all the relevant daemons. Useful post-upgrade
  message "(Re)starting Services..."
  as_root systemctl daemon-reload || warn "Could not reload systemd"
  as_root systemctl restart iptables ip6tables || warn "Could not restart iptables. check rules files"
  as_root systemctl restart iptables_user || warn "Could not restart iptables user rules, check rule file"
  as_root systemctl restart rsyslog || warn "Could not restart rsyslog"
  as_root systemctl restart psad || warn "Could not restart PSAD"
  as_root systemctl restart arp_watch || warn "arp_watch restart failed"
}

iptables_reload() {
  # just restart iptables
  as_root systemctl restart iptables ip6tables || warn "Could not reload iptables rules"
  as_root systemctl restart iptables_user || warn "Could not reload iptables user rules"
}

main() {
  local command="${1}" 
  case ${command} in
   enable)
     dep_check
     check_sudo
     ninja_enable
     ;;
   disable)
     dep_check
     check_sudo
     ninja_disable
     ;;
   update)
     dep_check
     check_sudo
     #[ ! -z ${2} ] && SNORT_RULES_URL="${2}"
     ninja_update
     ;;
   reload)
     dep_check
     check_sudo
     ninja_reload
     ;;
   iptables_update)
     iptables_reload
     ;;
   help)
     help_and_exit
     ;;
   *)
     help_and_exit
     ;;
  esac
}

main "${@}"
